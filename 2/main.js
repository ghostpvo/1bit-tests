var a = undefined, 
	b = undefined;

setTimeout(function(){
    a = 'a';
    console.log(a);
}, 1000 * Math.random());

setTimeout(function(){ 
    b = 'b';
    console.log(b);
}, 1000 * Math.random());

// Проблема заключалась в том, что в первом setTimeout выводилось значение переменной 'b', которая, на момент вывода, была undefined.
// Во втором все работало, поскольку переменной b было присвоено значение в первой функции.