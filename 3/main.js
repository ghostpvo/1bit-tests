var theItem = null;
var replaceItem = function () {
   	// var priorItem = theItem;
   	var writeToLog = function () {
       	if (theItem) { // priorItem заменено на theItem, значение условия сохранено, сокольку priorItem = theItem
           	console.log("hi");
       	}
   	};
	theItem = {
       	longStr: new Array(1000000).join('*'),
       	someMethod: function () {
           	console.log("someMessage");
       	}
   	};
};
setInterval(replaceItem, 1000);

// priorItem не отправляется в "мусорку", поскольку его использует функция writeToLog. 
// При каждом запуске replaceItem создаётся новый priorItem, но не удаляется, вследствии чего мы можем наблюдать "утечку памяти"