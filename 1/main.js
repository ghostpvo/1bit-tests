var SimpleCollapse = function(curObj) {
		this.objName = curObj;
		var stateChanger = '.collapse-control';

		// Vanilla JS edit
		this.init = function() {
			var elStateToggler = document.querySelector(this.objName + '>' + stateChanger),
				curBlk,
				toggleState = function() {
					curBlk.classList.toggle('contens-show');
				};

			elStateToggler.onclick = function(e) {
				e.preventDefault();
				curBlk = this.nextElementSibling;
				toggleState();
			};
		};

		// JQuery animate edit
		this.initAnimate = function() {
			$(stateChanger).click(function(e) {
				e.preventDefault();
				$(this).next().slideToggle();
			});
		};
	},
	obSimpleCollapse = new SimpleCollapse('.simple-collapse');

obSimpleCollapse.init();
// obSimpleCollapse.initAnimate(); 

// В случае тестирования initAnimate, не упустите из виду строку под номером 30 и обязательно закомментируйте ее ;)